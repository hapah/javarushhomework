package main.java.com.javarush.level16.lesson13.bonus01.common;

public enum ImageTypes implements ImageReader
{
    BMP,
    JPG,
    PNG;
}
