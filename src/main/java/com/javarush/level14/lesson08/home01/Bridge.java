package main.java.com.javarush.level14.lesson08.home01;

/**
 * Created by mucherinovnv on 26.01.2016.
 */
interface Bridge
{
    int getCarsCount();
}
