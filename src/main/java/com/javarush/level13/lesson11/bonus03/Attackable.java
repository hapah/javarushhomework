package main.java.com.javarush.level13.lesson11.bonus03;

public interface Attackable
{
    BodyPart attack();
}
