package main.java.com.javarush.level17.lesson10.bonus01;

public enum Sex {
    MALE,
    FEMALE
}
